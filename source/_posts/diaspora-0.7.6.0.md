
---
layout: "post"
title: "diaspora* 0.7.6.0 release"
date: 2018-06-28
tags:
    - diaspora
preview: "This version improves poll federation, diaspora emails, languages support, and refines account migration, that'll be added in the next major release"
url: "https://pod.diaspora.software/posts/1877755"
lang: en
---

New minor version of diaspora* released. It features improvements of poll federation, supporting more languages, posts cross-posted to Facebook will always include a link to the original post on diaspora*, emails sent by diaspora now include a link to the pod in the footer. Some technical improvements to most requested account migration feature landed in this diaspora* version. Work on it continues, the delivery of the complete migration feature is expected in the next major version.
Read the official team [announcement](https://pod.diaspora.software/posts/1877755).

You'll find full changelog [here](https://github.com/diaspora/diaspora/releases/tag/v0.7.6.0).

As a bonus, diaspora* community members Gibberish.org have created an XMPP [chat room](https://social.gibberfish.org/uploads/images/scaled_full_171c312e714793593ad2.png) for newcomers to the Federation to talk, ask questions, and get to know one another. Join it if you have questions about diaspora* or are looking for people to connect with.
