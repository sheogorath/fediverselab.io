
---
layout: "post"
title: "Hubzilla moves to GitLab"
date: 2018-06-07
tags:
    - hubzilla
preview: "Hubzilla project moved source code to Framasoft's GitLab instance. GitHub repos are archived. Remember to point your installation to new repository"
url: "https://hub.somaton.com/channel/mario/?f=&mid=52909e13dabe30274cbe792a357752883741cf57cff661cc8702aefb7b1483cc@hub.somaton.com"
lang: en
---

Hubzilla project [moved](https://hub.somaton.com/channel/mario/?f=&mid=52909e13dabe30274cbe792a357752883741cf57cff661cc8702aefb7b1483cc@hub.somaton.com) [source code](https://framagit.org/hubzilla/core) to [Framasoft](https://framasoft.org)'s GitLab instance. GitHub repos are archived and won't receive updates anymore but will stay intact for a short transition period. Don't forget to point your installation to new repository.
