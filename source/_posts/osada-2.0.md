
---
layout: "post"
title: "Osada 2.0 major release"
date: 2018-12-10
tags:
    - osada
preview: "Osada reaches version 2.0, which brings fully threaded conversations, forums compatible with ActivityPub and Twitter/StatusNet compatible API"
url: "https://osada.macgirvin.com/item/9a9033ff-f62f-4c53-a5f9-5c1e5dc93955"
lang: en
---

Osada reaches version 2.0, which brings fully threaded conversations, groups (forums) compatible with ActivityPub projects, and Twitter / StatusNet compatible API.

Osada is the new kid on the federating block and strives to be easy for general user, at the same time providing a range of additional features one can find in Hubzilla (network for power users).

[Official announcement](https://osada.macgirvin.com/item/9a9033ff-f62f-4c53-a5f9-5c1e5dc93955)
