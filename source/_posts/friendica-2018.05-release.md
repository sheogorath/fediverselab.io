
---
layout: "post"
title: "Friendica 2018.05 release"
date: 2018-06-02
tags:
    - friendica
preview: "This release includes bug fixes and enhancements, like hashtag autocomplete, automatic installations, terms of service module and translation updates"
url: "https://friendi.ca/2018/06/01/friendica-2018-05-released"
lang: en
---

This release includes some bug fixes and enhancements, like hashtag autocomplete, automatic installations, terms of service module and translation updates. You'll find more information and a full changelog [here](https://friendi.ca/2018/06/01/friendica-2018-05-released).
