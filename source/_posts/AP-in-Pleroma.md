
---
layout: "post"
title: "ActivityPub soon in Pleroma"
date: 2018-03-04
tags:
    - pleroma
preview: "ActivityPub support will be merged into the develop branch of Pleroma in the next days. Have fun sending pseudo-private messages to Mastodon friends!"
url: "https://blog.soykaf.com/post/activity-pub-in-pleroma"
lang: en
---

ActivityPub support will be merged into the develop branch of Pleroma in the next days. Have fun sending pseudo-private messages to Mastodon friends!
More info [here](https://blog.soykaf.com/post/activity-pub-in-pleroma).
