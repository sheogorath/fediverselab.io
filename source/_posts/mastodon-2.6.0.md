---
layout: "post"
title: "Mastodon 2.6.0 / 2.6.1"
date: 2018-10-31
tags:
    - mastodon
preview: "Several long-awaited additions in this Mastodon release, including 'read more' link on overly long in-stream toots and link previews"
url: "https://github.com/tootsuite/mastodon/releases/tag/v2.6.0"
lang: en
---

Several long-awaited additions in 2.6.0 Mastodon release, including "read more" link on overly long toots, and link previews. It also features link ownership verification and redesigned forms.

You'll find the full changelog [here](https://github.com/tootsuite/mastodon/releases/tag/v2.6.0).

A regression was found after the release, so you might as well go streight for the [2.6.1](https://github.com/tootsuite/mastodon/releases/tag/v2.6.1) hotfix.

This release adds limit for the number of people that can be followed from one account. Once upon a time administrators used "follow bots" to find and follow Fediverse users. The main goal was connecting to as many other servers as possible, improving the view of federated networks, getting more content into one's timeline. Today we have relays for this purpose: there's a solution from [Mastodon](https://source.joinmastodon.org/mastodon/pub-relay), and one from [Pleroma](https://git.pleroma.social/pleroma/relay), while diaspora-protocol networks have had [the relay system](https://relay.iliketoast.net) for a long time.

Still there are and will be accounts that follow tens of thousands of users. Some Fediverse administrators think collecting user data is their mission. Servers like Albin project openly state data tracking. One way to deal with this problem is adding limitations to the software. Another way is mass blocking by other administrators. Perhaps, the most reliable solution is to consider all data users share publicly to be public, potentially trackable by anyone. As opposed to private data — data not shared publicly and not stored as plain text.


Happy hacking. Sorry, happy Halloween!
